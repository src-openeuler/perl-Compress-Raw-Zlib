Name:      perl-Compress-Raw-Zlib
Epoch:     1
Version:   2.213
Release:   2
Summary:   Provides a Perl interface to the zlib compression library	
# source code in tarball have different license
# not used
# zlib-src: zlib
# Others:   GPL+ or Artistic
License:   GPL-1.0-or-later and zlib
URL:       https://metacpan.org/release/Compress-Raw-Zlib
Source0:   https://cpan.metacpan.org/authors/id/P/PM/PMQS/Compress-Raw-Zlib-%{version}.tar.gz

BuildRequires: gcc perl(ExtUtils::MakeMaker) perl-interpreter perl-generators zlib-devel
#for test
BuildRequires: perl(Test::Pod) >= 1.00 perl-devel
Requires:      perl(XSLoader)

%description
This module provides a Perl interface to the zlib compression library.

%package_help

%prep
%autosetup -n Compress-Raw-Zlib-%{version} -p1

%build
#change config as introduce in README
BUILD_ZLIB=False
ZLIB_INCLUDE=%{_includedir}
ZLIB_LIB=%{_libdir}
OLD_ZLIB=False
export BUILD_ZLIB ZLIB_INCLUDE ZLIB_LIB OLD_ZLIB
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}" NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test TEST_VERBOSE=1 COMPRESS_ZLIB_RUN_ALL=1

%files
%doc README
%{perl_vendorarch}/*

%files help
%doc Changes
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1:2.213-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Nov 18 2024 zhaosaisai <zhaosaisai@kylinos.cn> - 1:2.213-1
- upgrade version to 2.213
- add zlib-ng 2.1.7 & 2.2.0 & 2.2.1 to workflow
- remove Werror from clang legacy builds
- use -xc++ when building with C++
- add perl 5.40 to workflows

* Tue Jul 30 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 1:2.212-1
- upgrade version to 2.212
- Add author & absrtract to Makefile.PL & meta.*
- Fix for READMEmd target
- Fix build failure with "c++-compat" warnings

* Thu Dec 28 2023 renhongxun <renhongxun@h-partners.com> - 1:2.206-1
- upgrade version to 2.206

* Thu Jul 20 2023 renhongxun <renhongxun@h-partners.com> - 1:2.205-1
- upgrade version to 2.205

* Tue Oct 18 2022 renhongxun <renhongxun@h-partners.com> - 1:2.202-1
- upgrade version to 2.202

* Mon Nov 15 2021 yuanxin <yuanxin24@huawei.com> - 2.101-1
- upgrade version to 2.101

* Thu Jul 22 2021 liudabo <liudabo1@huawei.com> - 2.100-2
- remove gdb build dependency

* Wed Jan 27 2021 liudabo <liudabo1@huawei.com> - 2.100-1
- upgrade version to 2.100

*Thu Jul 23 2020 shixuantong<shixuantong@huawei.com> - 2.095-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to 2.095-1

* Wed May 13 2020 shenyangyang<shenyangyang4@huawei.com> - 1:2.081-7
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:add build requires of perl-devel and add Epoch for updating

* Thu Sep 5 2019 shenyangyang<shenyangyang4@huawei.com> - 2.081-6
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:modify license

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.081-5
- Package init
